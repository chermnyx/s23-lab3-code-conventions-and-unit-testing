package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

// testst for package com.hw.db.controllers.ThreadController class
public class ThreadControllerTest {
    private ThreadController threadController;
    private Thread thread;
    private Post post;
    private User user;

    @BeforeEach
    public void init() {
        threadController = new ThreadController();
        Timestamp timestamp = new Timestamp(0);
        thread = new Thread("author", timestamp, "forum", "message", "slug", "title", 0);
        thread.setId(420);
        post = new Post("author", timestamp, "forum", "message", 0, 0, false);
        user = new User("author", "email", "fullname", "about");
    }

    @Test
    @DisplayName("Test for CheckIdOrSlug method")
    public void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(420)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            Assertions.assertEquals(thread, threadController.CheckIdOrSlug("420"));
            Assertions.assertEquals(thread, threadController.CheckIdOrSlug("slug"));
        }
    }

    @Test
    @DisplayName("Test for createPost method")
    public void testCreatePost() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                mockedThreadDAO.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                mockedUserDAO.when(() -> UserDAO.Info("author")).thenReturn(user);
                ResponseEntity responseEntity = threadController.createPost("0", Collections.singletonList(post));
                Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
                Assertions.assertEquals(Collections.singletonList(post), responseEntity.getBody());
            }
        }
    }

    @Test
    @DisplayName("Test for Posts method")
    public void testPosts() {
        List<Post> posts = Collections.singletonList(post);
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(420)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getPosts(420, 1, 0, "flat", false)).thenReturn(posts);
            ResponseEntity responseEntity = threadController.Posts("slug", 1, 0, "flat", false);
            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(posts, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("Test for change method")
    public void testChange() {
        Thread thread1 = new Thread("author", new Timestamp(0), "forum", "message", "slug1", "title", 0);
        thread1.setId(666);
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(420)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(666)).thenReturn(thread1);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug1")).thenReturn(thread1);

            ResponseEntity responseEntity = threadController.change("slug1", thread1);
            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(thread1, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("Test for info method")
    public void testInfo() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(420)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            ResponseEntity responseEntity = threadController.info("slug");
            Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            Assertions.assertEquals(thread, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("Test for createVote method")
    public void testCreateVote() {
        Vote vote = new Vote("author", 1);
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
                mockedThreadDAO.when(() -> ThreadDAO.getThreadById(420)).thenReturn(thread);
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
                mockedUserDAO.when(() -> UserDAO.Info("author")).thenReturn(user);
                ResponseEntity responseEntity = threadController.createVote("slug", vote);
                Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                Assertions.assertEquals(thread, responseEntity.getBody());
            }
        }
    }
}
